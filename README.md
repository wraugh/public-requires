I'm developing a style of documentation that involves plenty of examples
pulled directly from a project's test suite, with the help of tools like
[snippin](https://www.npmjs.com/package/snippin) and
[marcco](https://www.npmjs.com/package/marcco). The examples can be safely
copied-and-pasted because they are guaranteed to work by the test suite.

Except when they contain package-local require statements.

So here's **public-requires**: it's a text transform that replaces
project-local require expressions, like

~~~javascript
require('../index.js')
~~~

by their public equivalent, i.e.

~~~javascript
require('my-library')
~~~

This makes your examples truly copy-pastable.

How it works
------------

**public-requires** assumes your code is written in [standard style](
https://standardjs.com/) with at most one *require* expression per line of
source code. That is, to keep things simple it uses a regex to find
requires: `/require\('(\.[^']*)'\)/`. If you require a relative path (i.e. if
the regex matches), it will resolve and rewrite it, taking into account the
`main` config in your package.json, if any. It's quick and dirty; it only
works if your code follows a standard, regular style. If it doesn't work for
you, please send a patch.

Example
-------

Suppose you have a file called `example.js` with this content:

    // example.js
    const myFunction = require('./index.js')
    tap.equals(myFunction('foo'), 'bar')

There are two ways to rewrite that `require` expression: 1) with a stream
transform:

~~~javascript
const expectedOutput = `// example.js
  const myFunction = require('your-project-name')
  tap.equals(myFunction('foo'), 'bar')
  `.replace(/^\s*/gm, '')

const { PublicRequires } = require('public-requires')
tap.test('Process example.js using the PublicRequires stream transform', t =>
  fs.createReadStream('./example.js')
    .pipe(new PublicRequires('your-project-name', __dirname, __dirname))
    .pipe(concat({}, result => {
      t.equals(result.toString(), expectedOutput)
      t.end()
    })))

~~~

or 2) with a function that operates on a string:


~~~javascript
const { rewriteRequires } = require('public-requires')
const input = fs.readFileSync('./example.js', 'utf8')
const output = rewriteRequires('your-project-name', __dirname, __dirname, input)
tap.equals(output, expectedOutput)
~~~


API
---

**rewriteRequires(packageName, projectDir, fileDir, fileContent)**

 - `packageName` *String* the public name of your package, i.e. how people
    refer to it when writing require statements
 - `projectDir` *String* the root directory of your project, where you would
    put your package.json file
 - `fileDir` *String* the directory of the file being rewritten; paths will
    be resolved relative to this directory
 - `fileContent` *String* the source code to rewrite
 - Returns: *String*

Returns `fileContent` except every time there's a require expression with a
relative path, it's a require expression with a public path instead.

**new PublicRequires(packageName, projectDir, fileDir, options = {})**

The stream transform version of `rewriteRequires`. The arguments mean the
same thing. `options` is passed as-is to the [stream.Transform constructor](
https://nodejs.org/api/stream.html#stream_new_stream_transform_options).


Contributing
------------

You're welcome to contribute to this project following the
[C4 process](https://rfc.zeromq.org/spec:42/C4/).

All patches must follow [standard style](https://standardjs.com/) and have 100%
test coverage. You can make sure of this by adding

    ./.pre-commit

to your git pre-commit hook.
