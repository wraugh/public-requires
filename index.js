/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
'use strict'

const fs = require('fs')
const path = require('path')
const pumpify = require('pumpify')
const { Transform } = require('readable-stream')
const split2 = require('split2')
const { StringDecoder } = require('string_decoder')

const requireRe = /require\('(\.[^']*)'\)/
const indexRe = /\/index(?:\.js|\.json|\.node)?$/

class _PublicRequires {
  constructor (packageName, projectDir, fileDir) {
    this._packageName = packageName
    this._projectDir = path.resolve(projectDir)
    this._fileDir = path.resolve(fileDir)
  }

  transformLine (line) {
    const m = requireRe.exec(line)
    if (m !== null) {
      const publicPath = this._rewrite(m[1])
      if (publicPath !== m[1]) {
        line = line.replace(m[1], publicPath.replace(/'/g, "\\'"))
      }
    }

    return line
  }

  _rewrite (reqPath) {
    const absPath = this._normalizePath(reqPath)
    if (absPath.slice(0, this._projectDir.length) !== this._projectDir) {
      return reqPath
    }

    let publicPath = this._shortenRequirePath(
      this._packageName + absPath.slice(this._projectDir.length))

    if (publicPath === this._getMainPath()) {
      publicPath = this._packageName
    }

    return publicPath
  }

  _getMainPath () {
    if (this._mainPath === undefined) {
      try {
        const packageJson = JSON.parse(fs.readFileSync(path.join(this._projectDir, 'package.json'), 'utf8'))
        if (packageJson.main) {
          this._mainPath = this._shortenRequirePath(`${this._packageName}/${packageJson.main}`)
        }
      } catch (e) {
        // Maybe we don't have a package.json, or it's not valid JSON. Ignore.
        this._mainPath = null
      }
    }

    return this._mainPath
  }

  _normalizePath (p) {
    p = path.resolve(this._fileDir, p)

    /* istanbul ignore if */
    if (path.sep === '\\') {
      p = p.replace(/\\/g, '/')
    }

    return p
  }

  _shortenRequirePath (p) {
    if (indexRe.test(p)) {
      p = p.replace(indexRe, '')
    }

    return p.replace(/\.(?:js|json|node)$/, '')
  }
}

// #snip "api"
// **rewriteRequires(packageName, projectDir, fileDir, fileContent)**
//
//  - `packageName` *String* the public name of your package, i.e. how people
//     refer to it when writing require statements
//  - `projectDir` *String* the root directory of your project, where you would
//     put your package.json file
//  - `fileDir` *String* the directory of the file being rewritten; paths will
//     be resolved relative to this directory
//  - `fileContent` *String* the source code to rewrite
//  - Returns: *String*
//
// Returns `fileContent` except every time there's a require expression with a
// relative path, it's a require expression with a public path instead.
// #snip
function rewriteRequires (packageName, projectDir, fileDir, srcCode) {
  const pr = new _PublicRequires(packageName, projectDir, fileDir)
  const lines = srcCode.split(/\n/)

  return lines.map(line => pr.transformLine(line)).join('\n')
}

// #snip "api"
//
// **new PublicRequires(packageName, projectDir, fileDir, options = {})**
//
// The stream transform version of `rewriteRequires`. The arguments mean the
// same thing. `options` is passed as-is to the [stream.Transform constructor](
// https://nodejs.org/api/stream.html#stream_new_stream_transform_options).
// #snip
function PublicRequires (packageName, projectDir, fileDir, options = {}) {
  const pr = new _PublicRequires(packageName, projectDir, fileDir)
  const tx = new Transform(Object.assign({}, options, {
    transform (line, encoding, callback) {
      if (this._decoder === undefined) {
        this._decoder = new StringDecoder(this._writableState.defaultEncoding)
      }
      callback(null, pr.transformLine(this._decoder.write(line)))
    }
  }))

  return pumpify(split2(/(\n)/, options), tx)
}

module.exports.rewriteRequires = rewriteRequires
module.exports.PublicRequires = PublicRequires
module.exports._requireRe = requireRe
