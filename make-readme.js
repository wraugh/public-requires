#!/usr/bin/env node
/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
'use strict'

// This script writes README.md from the doc snippets in the codebase

const fs = require('fs')
const { code2doc } = require('marcco')
const snippin = require('snippin')
const ST = require('stream-template')
const { rewriteRequires, _requireRe } = require('.')

// presentation helpers
const rr = src => rewriteRequires('public-requires', __dirname, __dirname, src)
const js2doc = src => code2doc(rr(src), { codePrefix: '~~~javascript' })

process.chdir(__dirname)
const out = fs.createWriteStream('./README.md')
const s = snippin()

Promise.all([s.getSnippets('./index.js'), s.getSnippets('./test.js')])
  .then(([index, test]) =>
    ST`I'm developing a style of documentation that involves plenty of examples
pulled directly from a project's test suite, with the help of tools like
[snippin](https://www.npmjs.com/package/snippin) and
[marcco](https://www.npmjs.com/package/marcco). The examples can be safely
copied-and-pasted because they are guaranteed to work by the test suite.

Except when they contain package-local require statements.

So here's **public-requires**: it's a text transform that replaces
project-local require expressions, like

~~~javascript
require('../index.js')
~~~

by their public equivalent, i.e.

~~~javascript
require('my-library')
~~~

This makes your examples truly copy-pastable.

How it works
------------

**public-requires** assumes your code is written in [standard style](
https://standardjs.com/) with at most one *require* expression per line of
source code. That is, to keep things simple it uses a regex to find
requires: \`${_requireRe.toString()}\`. If you require a relative path (i.e. if
the regex matches), it will resolve and rewrite it, taking into account the
\`main\` config in your package.json, if any. It's quick and dirty; it only
works if your code follows a standard, regular style. If it doesn't work for
you, please send a patch.

Example
-------

${test.exampleSetup}
${js2doc(test.example)}

API
---

${index.api}

Contributing
------------

You're welcome to contribute to this project following the
[C4 process](https://rfc.zeromq.org/spec:42/C4/).

All patches must follow [standard style](https://standardjs.com/) and have 100%
test coverage. You can make sure of this by adding

    ./.pre-commit

to your git pre-commit hook.
`.pipe(out))
