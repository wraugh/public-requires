/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
'use strict'

const concat = require('concat-stream')
const fs = require('fs')
const snippin = require('snippin')
const tap = require('tap')
const tmp = require('tmp')

// #snip "exampleSetup"
// Suppose you have a file called `example.js` with this content:
//
//     #snip "example.js"
//     // example.js
//     const myFunction = require('./index.js')
//     tap.equals(myFunction('foo'), 'bar')
//     #snip
// #snip

// Yes indeed, suppose we do have such a file
const s = snippin()
const theseSnippets = s.getSnippets(__filename)
tap.test('example wrapper', tap => {
  theseSnippets.then(snippets => {
    fs.writeFileSync('./example.js', snippets['example.js'])

    // #snip "example"
    // There are two ways to rewrite that `require` expression: 1) with a stream
    // transform:
    const expectedOutput = `// example.js
      const myFunction = require('your-project-name')
      tap.equals(myFunction('foo'), 'bar')
      `.replace(/^\s*/gm, '')

    const { PublicRequires } = require('.')
    tap.test('Process example.js using the PublicRequires stream transform', t =>
      fs.createReadStream('./example.js')
        .pipe(new PublicRequires('your-project-name', __dirname, __dirname))
        .pipe(concat({}, result => {
          t.equals(result.toString(), expectedOutput)
          t.end()
        })))

    // or 2) with a function that operates on a string:

    const { rewriteRequires } = require('.')
    const input = fs.readFileSync('./example.js', 'utf8')
    const output = rewriteRequires('your-project-name', __dirname, __dirname, input)
    tap.equals(output, expectedOutput)
    // #snip

    fs.unlinkSync('./example.js')
    tap.end()
  })
})

// There should be no difference between a project with no package.json,
// one that doesn't have a "main" property, and one where it says "index.js".
// We'll create temporary directories to test the first and last case, and
// use this current project for the middle case.
const { PublicRequires, rewriteRequires } = require('.')
const emptyDir = tmp.dirSync().name
const tmpDir = tmp.dirSync().name
fs.writeFileSync(`${tmpDir}/package.json`, '{"main":"index.js"}')
for (let dir of [emptyDir, __dirname, tmpDir]) {
  tap.equals(rewriteRequires('foobar', dir, dir, "require('sanity-check')"),
    "require('sanity-check')")

  tap.equals(rewriteRequires('foobar', dir, dir, "require('.')"),
    "require('foobar')")
  tap.equals(rewriteRequires('foobar', dir, dir, "require('./')"),
    "require('foobar')")
  tap.equals(rewriteRequires('foobar', dir, dir, "require('./index')"),
    "require('foobar')")
  tap.equals(rewriteRequires('foobar', dir, dir, "require('./index.js')"),
    "require('foobar')")

  tap.equals(rewriteRequires('foobar', dir, dir, "require('./test')"),
    "require('foobar/test')")
  tap.equals(rewriteRequires('foobar', dir, dir, "require('./test.js')"),
    "require('foobar/test')")

  tap.equals(rewriteRequires('foobar', dir, dir, "require('./file/need/not/exist')"),
    "require('foobar/file/need/not/exist')")
  tap.equals(rewriteRequires('foobar', dir, dir, "require('./file/need/not/exist.js')"),
    "require('foobar/file/need/not/exist')")
  tap.equals(rewriteRequires('foobar', dir, dir, "require('./file/../inexistant')"),
    "require('foobar/inexistant')")

  tap.equals(rewriteRequires('foobar', dir, dir, "require('../outside/of/project/dir.js')"),
    "require('../outside/of/project/dir.js')")

  tap.equals(rewriteRequires('foobar', dir, `${dir}/foo`, "require('..')"),
    "require('foobar')")
  tap.equals(rewriteRequires('foobar', dir, `${dir}/foo`, "require('../')"),
    "require('foobar')")
  tap.equals(rewriteRequires('foobar', dir, `${dir}/foo`, "require('../index')"),
    "require('foobar')")
  tap.equals(rewriteRequires('foobar', dir, `${dir}/foo`, "require('../index.js')"),
    "require('foobar')")

  tap.equals(rewriteRequires('foobar', dir, `${dir}/foo`, "require('../test')"),
    "require('foobar/test')")
  tap.equals(rewriteRequires('foobar', dir, `${dir}/foo`, "require('../test.js')"),
    "require('foobar/test')")
}

// Now let's make a package.json with a "main" other than "index.js"
fs.writeFileSync(`${tmpDir}/package.json`, '{"main":"main.js"}')
let src = `
  require('.')
  require('./')
  require('./main')
  require('./main.js')
  require('./main.json')
  require('./index.js') // This file doesn't exist, but public-requires doesn't care. It assumes your code works.
`
let out = `
  require('foobar')
  require('foobar')
  require('foobar')
  require('foobar')
  require('foobar')
  require('foobar') // This file doesn't exist, but public-requires doesn't care. It assumes your code works.
`
tap.equals(rewriteRequires('foobar', tmpDir, tmpDir, src), out)

// And with "main" being a directory
fs.writeFileSync(`${tmpDir}/package.json`, '{"main":"src"}')
src = `
  require('.')
  require('./')
  require('./src')
  require('./src/index')
  require('./src/index.js')
  require('./src/index.json')
  require('./src/main.js')
  require('./main.js')
  require('./index.js')
`
out = `
  require('foobar')
  require('foobar')
  require('foobar')
  require('foobar')
  require('foobar')
  require('foobar')
  require('foobar/src/main')
  require('foobar/main')
  require('foobar')
`
tap.equals(rewriteRequires('foobar', tmpDir, tmpDir, src), out)

tap.test('The stream interface should work the same way', t => {
  let actual = ''
  const tx = new PublicRequires('foobar', tmpDir, tmpDir)
  tx.on('data', line => {
    actual += line
  })
  tx.on('finish', () => {
    t.equals(actual, out)
    t.end()
  })
  tx.end(src)
})

tap.test('We can pass in stream options', t => {
  let actual = ''
  const tx = new PublicRequires('foobar', tmpDir, tmpDir, { highWaterMark: 2 })
  tx.on('data', line => {
    actual += line
  })
  tx.on('end', () => {
    t.equals(actual, out)
    t.end()
  })
  tx.end(src)
})
